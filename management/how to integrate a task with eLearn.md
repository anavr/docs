# How to integrate ‘Autocode’ task with ‘eLearn’ system

## Create a course
1. Log in to 'Autocode' as a trainer

2. Go to the “Course list” tab and click on the [Create course] button


	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How_to_integrate_task.001.png)

3. Fill the required fields and click on the [Save] button


	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How_to_integrate_task.002.png)

4. Click on the [Create topic] button. Fill required fields and click on the [Save] button


	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How_to_integrate_task.003.png)

5. Create a task.      


	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How_to_integrate_task.005.png)

6. Fill the required fields and click on the [Save] button 

7. Click on the [Back] button and change course status to “Published”


	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How_to_integrate_task.006.png)

## Create a group
1. Go to the “Group list” tab and click on the [Create group] button
	
	
	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How_to_integrate_task.007.png)

2. Fill required fields and click on the [save] button 

3. Group creates with “Draft” status. Change group status to “In progress”: 
	- Click on the [Open registration] button


		![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How%20to%20integrate%20task.008.png)

	- Click on the [Start group] button


		![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How%20to%20integrate%20task.009.png)

## Integration
1. Open a specified group

2. Open the “LTI Passport” tab and click on the [Create LTI passport] button


	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How%20to%20integrate%20task.010.png)

3. Get access to the eLearn course. Open an "Instructor" dashboard in the course and click on the [View Course in Studio] button
	
	
	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How%20to%20integrate%20task.011.png)

4. Click on the course name in the list of available courses
	
	
	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How%20to%20integrate%20task.012.png)

5. Create new section, Subsection, Unit. After clicking on the [New Unit], unit screen opens
	
	
	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How%20to%20integrate%20task.013.png)

6. Open the “Advanced Settings” screen                            
	
	
	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How%20to%20integrate%20task.014.png)

7. Paste the “lti_consumer” string to the “Advanced Module List”
	
	
	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How%20to%20integrate%20task.015.png)

8. Copy “Passport string” from “Autocode” to Advanced settings--> “LTI Passports” and insert it in quotes. Click on the [Save changes] button
	
	
	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How%20to%20integrate%20task.016.png)

9. Open a unit screen. Advanced option appears                             
	
	
	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How%20to%20integrate%20task.017.png)

10. Click on the “Advanced”, then click on the “LTI Consumer”, then click on the [Edit] button
	
	
	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How%20to%20integrate%20task.018.png)

11. Paste part of the “LTI Passport” string before a colon as “LTI ID”. Paste a “Launch URL” as “LTI URL”. Paste “LTI custom parameters” as “Custom Parameters”
	
	
	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How%20to%20integrate%20task.019.png)

	
	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How%20to%20integrate%20task.020.png)

12. Set "True" in the "Scored" field. Enter the number of point possible for specified component in the "Weight" field. Click on the [Save] button.


	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How%20to%20integrate%20task.022.png)

13. Check task integrates successfully                                            
	
	
	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How%20to%20integrate%20task.021.png)

14. Open the "Grading" screen. Create assignment type.


	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How%20to%20integrate%20task.023.png)


	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How%20to%20integrate%20task.024.png)

15. Set a configured assignment type to specified subsections of the course


	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How%20to%20integrate%20task.025.png.png)


	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How%20to%20integrate%20task.026.png)

16. To check a student results go through the 'Instructor' dashboard and click on the [View Gradebook] button


	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How%20to%20integrate%20task.027.png)


	![](https://gitlab.com/autocodebot/docs/-/raw/master/management/How%20to%20integrate%20task.028.png)	
