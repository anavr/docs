## Features list of Autocode v2.0

 **Git integration** 

   - User can authorize 'Autocode' account with GitLab
   - User can authorize 'Autocode' account with GitHub
   - User can deauthorize GitHub/GitLab account
 

 ### User authorities (Every user can have multiple authorities)

   - The system stores the predefined list of authorities allowing users to perform different actions in the system:


 #### **ANONYMOUS** - limited read-only access to public courses and tasks.

   - User can browse course/tasks on the 'Explore' page
   - User can sign in to the 'Autocode'


 #### **USER** - An authenticated user that can enroll in courses and take tasks. 

   - Every user registered in the system has **USER** authority by default.
   - User can participate in a course as [**MENTOR**](https://gitlab.com/autocodebot/docs/-/blob/master/%5B2.0%5D/features%20list.md#course-contributor-role-mentor) or [**COORDINATOR**](https://gitlab.com/autocodebot/docs/-/blob/master/%5B2.0%5D/features%20list.md#course-contributor-role-coordinator) or [**MAINTAINER**](https://gitlab.com/autocodebot/docs/-/blob/master/%5B2.0%5D/features%20list.md#course-contributor-role-maintainer).

 **Explore / My learning/ My contribution**
 
   - User can open a course/task and get info about it
   - User can switch between the courses/tasks tab
   - User can search a course/task
   - User can filter the courses/tasks
   - User can sort a page content
   - User can page a page 
   - User can page back a page
   - User can change a number of items per page
   - User can change a view of the courses/tasks
   - User can change a language EN/RU


 **Enrollments**

   - User can enroll into a **public** couse
   - User can accept an enrollment to a **Private** course via invitation link
   - User can cancel an enrollment request (manual enroll. type of the course)
   - User can leave the course


 **Assignments**
 
   - User can start a course/task assignment

   > User cannot start a course/task assignment without GitLab/GitHub authorization

   - User can submit a course/task assignment results
   - User can receive a course/task assignment result grades
   - User can finish a course/task assignment

   > a user enrollment status should be 'Included' to have permissions to the described actions in the course

   - User can resume a course task assignment after review

   > the assignment review flag is on

   - User can resume a course task assignment after finish

   > the assignment review flag is off

 
 **Help**

   - User can view the help page


 **Profile**

   - User can Log Out from the 'Autocode'


 #### **TRAINER** - a user with rights to create new courses and tasks.

 > This role provides all actions of the USER role and below list of the additional features:


 **My contribution**

   - User can create a task
   - User can create a course
   - User can specify the owned courses/tasks


 **Check task**

   - User can specify the details to check a repository
   - User can view the results of the all own builds

 **Help** 

   - User can see help page for the TRAINER


 #### **ADMIN** - full access to the system including system dictionaries and user management (granting authorities).

 > This role provides all actions of the USER role and below list of the additional features:

 **Explore / My learning/ My contribution**

   - User can browse any courses and tasks


 **Administration**

   - User can see the 'Users' list
   - User can change users' authorities
   - User can grant 'TRAINER', 'ADMIN' authority
   - User can revoke 'TRAINER', 'ADMIN' authority
   - User cannot revoke the 'USER' authority
   - User cannot edit own authority
   - User can create a tag
   - User can edit a tag
   - User can delete a tag
   - User can browse the all builds of the users (Monitoring)
   - User can browse the Qulity Profiles


 **Check task**

   - User can specify the details to check a repository
   - User can view the results of the all own builds


 **Help** 

   - User can see help page for the ADMIN


 ### COURSE CONTRIBUTORS


 #### COURSE CONTRIBUTOR ROLE **MAINTAINER**

  **Course actions (MAINTAINER)**

   - User can edit the common fields of the course 
   - User can edit the "Details" of the course
   - User can edit the "Scoring details" of the course
   - User can publish a course
   - User can start a course
   - User can cancel a course
   - User can modify course structure
   - User can finish a course
   - User can clone a course
   - User can export course data


  **Course structure (MAINTAINER)**

   - User can create a new module
   - User can create a new task in module
   - User can create a new task without module
   - User can update a task/module
   - User can reorder a task/module
   - User can delete a task/module
   - User can activate a task
   - User can deactivate a task
   - User can check a task
   - User can clone a task


  **Course enrollments (MAINTAINER)**

   - User can invite a user
   - User can delete invitation
   - User can approve an enrollment
   - User can reject an enrollment
   - User can exclude an enrollment
   - User can view an enrollment changes history
   - User can filter the enrollments
   - User can search a user
   - User can create a label
   - User can delete a label
   - User can assign a label
   - User can replace an existed label


  **Course assignments (MAINTAINER)**

   - User can approve an assignment
   - User can reject an assignment
   - User can get access to a student's repository
   - User can view an assignment changes history
   - User can view a build details


  **Course contributors (MAINTAINER)**

   - User can add a contributor to a course
   - User can change a contributor role
   - User can unassign a contributor from a course
   - User can search a contributor from the list
   - User can filter the contributors


 #### COURSE CONTRIBUTOR ROLE **COORDINATOR**

 > read-write access to all student assignments, course structure and course enrollments within a single course


  **Course actions (COORDINATOR)**

   - User can export course data


  **Course structure (COORDINATOR)**

   - User can view the structure


  **Course enrollments (COORDINATOR)**

   - User can invite a user
   - User can delete invitation
   - User can approve an enrollment
   - User can reject an enrollment
   - User can exclude an enrollment
   - User can view an enrollment changes history
   - User can filter the enrollments
   - User can search a user
   - User can create a label
   - User can delete a label
   - User can assign a label
   - User can replace an existed label


  **Course assignments (COORDINATOR)**

   - User can approve an assignment
   - User can reject an assignment
   - User can get access to a student's repository
   - User can view an assignment changes history
   - User can view a build details


  **Course contributors (COORDINATOR)**

   - User can search a contributor from the list
   - User can filter the contributors

 
 #### COURSE CONTRIBUTOR ROLE **MENTOR**

  **Course actions (MENTOR)** 
 
   > read-write access to the assigned students assignments within a single course.


  **Course actions (MENTOR)**

   - User can export course data


  **Course structure (MENTOR)**

   - User can view the structure?


 **Course enrollments (MENTOR)**

   - User can approve an assigned student enrollment
   - User can reject an assigned student enrollment
   - User can exclude an assigned student enrollment
   - User can view an assigned student enrollment changes history
   - User can filter the assigned students enrollments
   - User can search a user within the assigned students
   - User can create a label?
   - User can delete a label?
   - User can assign a label?
   - User can replace an existed label?


  **Course assignments (MENTOR)**

   - User can approve an assigned student assignment
   - User can reject an assigned student assignment
   - User can get access to an assigned student student's repository
   - User can view an assigned student assignment changes history
   - User can view an assigned student build details
 

 ### Courses actions

| ACTION | AVAILABLE IN STATUSES | DETAILS |
| ------ | ------ | ------ |
| Create | N/A | Initial action for creating a course in "Draft" status |
| Publish |	Draft | Performed by the course owner and contributors to make the course available |
| Start | Published	| Performed by the course owner and contributors to start the course |
| Invite | Published/In progress | Performed by the course owner and contributors to generate an invitation link |
| Delete invitation | Published/In progress | Performed by the course owner and contributors to delete an invitation link |
| Enroll | Published/In progress | Performed by users who want to enroll in the course |
| Finish | In progress | Performed by the course owner and contributors to finish the course |
| Cancel | Published | Performed by the course owner and contributors to cancel the course |
| Update | Draft/Published/In progress | Performed by the task owner and contributors to update course main information |
| Update details | Draft/Published/In progress | Performed by the task owner and contributors to update course details information |
| Clone | All |	Performed by the task owner and contributors to clone the course |
