# Student guide

## CONTENTS
1. REGISTRATION
2. GITLAB INTEGRATION
3. COURSE SUBSCRIBING	
4. TASK SUBMITTING

## 1. REGISTRATION
Social Login supposed to use any social network but performing a study project in Autocode is carried out through using GitLab, so you must have your own account on Gitlab (gitlab.com).

Open [autocode.lab.epam.com](autocode.lab.epam.com)

1. Click on the ‘Social Login’ link on the top right corner
      
      
      ![](https://gitlab.com/autocodebot/docs/-/raw/master/students%20information/001.png)

2. Click on the ‘Sign in with GitLab.com’

      
      ![](https://gitlab.com/autocodebot/docs/-/raw/master/students%20information/002.png)

3. Log in to your GitLab account

4. Accept Privacy note

## 2. GITLAB INTEGRATION
You should integrate your Autocode account with GitLab to auto fork the original project and then check the solution. 

The original project should be forked to your GitLab account to let you work in your own repository without affecting the original (trainer’s) project.

Fork - a copy of the original project.

1. Click on the ‘View profile’ in user menu

      
      ![](https://gitlab.com/autocodebot/docs/-/raw/master/students%20information/003.png)

2. Click on the ‘INTEGRATE GITLAB’ button

      
      ![](https://gitlab.com/autocodebot/docs/-/raw/master/students%20information/004.png)

3. Confirm by clicking ‘Authorize’

      
      ![](https://gitlab.com/autocodebot/docs/-/raw/master/students%20information/005.png)

## 3. COURSE SUBSCRIBING
1. Open ‘Available courses’ tab

      
      ![](https://gitlab.com/autocodebot/docs/-/raw/master/students%20information/006.png)

2. Click on the ‘SUBSCRIBE’ button

      
      ![](https://gitlab.com/autocodebot/docs/-/raw/master/students%20information/007.png)

If a course has parameter ‘Auto Assign’ you will be assigned automatically.

If a course has parameter ‘Manual Assign’, please wait for the trainer`s approval.

As soon as the course is available to you, you will see it on ‘IN PROGRESS’ tab.

## 4. TASK SUBMITTING
After the student is confirmed to the course, the student can send homework for review:

1. Go to the course page (through 'My courses')

2. On the left side, the tasks you can submit for review

      
      ![](https://gitlab.com/autocodebot/docs/-/raw/master/students%20information/008.png)

3. Click on the [Start] button to transfer the task to IN PROGRESS

      
      ![](https://gitlab.com/autocodebot/docs/-/raw/master/students%20information/009.png)

**If the deadline has not come or has ended - button be unavailable**

4. The project is auto forked and the “Your repository” link appears, forked project is private, @autocodebot is added to your project fork automatically.
      
      
      ![](https://gitlab.com/autocodebot/docs/-/raw/master/students%20information/010.png)

5. **GitLab:** Continue working in your own repository without affecting the original (trainer’s) project.

6. **GitLab:** Press [Edit] and enter your prepared solution of the task.

      
      ![](https://gitlab.com/autocodebot/docs/-/raw/master/students%20information/011.png)

7. On the task page, click the [Check task] button

      
      ![](https://gitlab.com/autocodebot/docs/-/raw/master/students%20information/012.png)

8. After several minutes you get results of each stage that your task passed

      
      ![](https://gitlab.com/autocodebot/docs/-/raw/master/students%20information/013.png)

9. For details of UNIT\_TESTS click [Details] and you will get detailed information in tabs SUMMARY and DETAIL

      
      ![](https://gitlab.com/autocodebot/docs/-/raw/master/students%20information/014.png)

10. For details of SONAR click [Details] and you will get detailed information in tabs SUMMARY and DETAIL

      
      ![](https://gitlab.com/autocodebot/docs/-/raw/master/students%20information/015.png)

11. If you want to improve your score you can submit task again and again according to the number of attempts your trainer allowed you. In this case you need edit your current solution in IDE, repeat operations for push code to GitLab and click ‘CHECK TASK’ button again.
